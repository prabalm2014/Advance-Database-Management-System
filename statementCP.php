<?php
	require("pdf/fpdf.php");
	$ids = $_GET['ac'];
	$con = odbc_connect('BMS', 'system', 'prabal');
	if(!$con)
	{
		die('Database not connected!');
	}
	$sql = "SELECT * FROM transection where ACCOUNTNO = '".$ids."' ORDER BY T_ID DESC";
	$result = odbc_exec($con, $sql);

	$pdf = new FPDF('P','mm','Letter');
	$pdf->SetMargins(16,10);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	

	$pdf->SetFont("Arial","B",12);
	$pdf->Cell(0,10,"Statement Report",0,1,'C');
	//$pdf->Ln();

	$pdf->Cell(30,10,"Account No",1,0,'C');
	$pdf->Cell(30,10,"Deposit",1,0,'C');
	$pdf->Cell(30,10,"Withdraw",1,0,'C');
	$pdf->Cell(30,10,"Balance",1,0,'C');
	$pdf->Cell(18,10,"Day",1,0,'C');
	$pdf->Cell(23,10,"Time",1,0,'C');
	$pdf->Cell(23,10,"Date",1,1,'C');

	
	while($row = odbc_fetch_array($result)) 
	{
		$pdf->SetFont("Arial","B",8);
		$pdf->Cell(30,10,$row['ACCOUNTNO'],1,0,'C');
		$pdf->Cell(30,10,$row['DEPOSIT'],1,0,'C');
		$pdf->Cell(30,10,$row['WITHDRAW'],1,0,'C');
		$pdf->Cell(30,10,$row['TOTAL'],1,0,'C');
		$pdf->Cell(18,10,$row['TODAY'],1,0,'C');
		$pdf->Cell(23,10,$row['TOTIME'],1,0,'C');
		$pdf->Cell(23,10,$row['TODATE'],1,1,'C');
	}

	$pdf->Output();

?>