<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>User Panel</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">User Panel</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-4">
					<ul class="nav navbar-nav">
						<li><a href="updatePass.php">Update Password</a></li>
						<li><a href="balanceCust.php">Balance</a></li>
						<li><a href="statementCust.php">Statement</a></li>
						<li><a href="topup.php">Mobile Topup</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	
	<?php 
		$con = odbc_connect('BMS', 'system', 'prabal');
		if(!$con)
		{
			die('Database not connected!');
		}
		$ui = $_SESSION['username'];
		$sql = "SELECT * FROM users where accountno='".$ui."' ";
		$result = odbc_exec($con, $sql);
		$row = odbc_fetch_array($result);
		echo "<br/><br/><h1 align=center>Welcome ".$row['FULLNAME']."</h1>";
	?>
	
	</div>
</body>
</html>