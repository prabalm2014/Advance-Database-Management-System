<?php
	session_start();
	$account =$customerid =$customeric =$actype =$currency =$initial ="";
	$Err="";

	$fname = $_SESSION["fname"] ;
	$gender = $_SESSION["gender"] ;
	$address = $_SESSION["address"] ;
	$phone = $_SESSION["phone"];
	$account = $_SESSION["account"];
	$password = $_SESSION["password"] ;

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$ok=1;
		if(empty($_POST["account"]) && empty($_POST["customerid"]) && empty($_POST["customeric"]) && empty($_POST["actype"]) && empty($_POST["currency"]) && empty($_POST["initial"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$account = test_input($_POST["account"]);
			$customerid = test_input($_POST["customerid"]);
			$customeric = test_input($_POST["customeric"]);
			$actype = test_input($_POST["actype"]);
			$currency = test_input($_POST["currency"]);
			$initial = test_input($_POST["initial"]);
		}
		if(!empty($_POST["submit"]))
		{	
			$con = odbc_connect('BMS', 'system', 'prabal');
			if(!$con)
			{
				die('Database not connected!');
			}
			$regex = "/^[0-9]{3}.[0-9]{4}.[0-9]{3}$/";	
			if(preg_match($regex, $account))
			{
				$fname = $_SESSION["fname"] ;
				$gender = $_SESSION["gender"] ;
				$address = $_SESSION["address"] ;
				$phone = $_SESSION["phone"];
				$account = $_SESSION["account"];
				$password = $_SESSION["password"] ;
				$sql = "INSERT INTO account VALUES ('".$account."','".$customerid."','".$customeric."','".$actype."','".$currency."')";
				odbc_exec($con, $sql);
				
				$sql2 = "INSERT INTO users VALUES (users_sequence.nextval,'".$fname."','".$gender."','".$address."','".$phone."','".$account."','".$password."')";
				odbc_exec($con, $sql2);

				date_default_timezone_set("Asia/Dhaka");
				$time = date("h:i:sa");
				$dat = date("Y/m/d");
				$mydate = getdate(date("U"));
				$day = "$mydate[weekday]";

				$sql3 = "INSERT INTO transection VALUES (transection_sequence.nextval,'".$account."','".$initial."',null,'".$initial."','".$day."','".$time."','".$dat."')";
				odbc_exec($con, $sql3);


				header('Location: admin.php');
				exit();
			}
			else{
				$Err= "&#9932 Username Should Be XXX.XXXX.XXX ";$ok=0;
				$account ="";
			}
			
		}
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Create Account|Account Information</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">Create An Account</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-9">
					<ul class="nav navbar-nav">
						<li><a href="admin.php">Admin</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Help</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	    <div class="col-md-6 forms col-md-offset-3">
	    	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>"><br/>
	    		<h1 class="h1-2 col-md-offset-2">Create Account</h1><br/>
	    		<input type="text" name="account" required placeholder="Account No" class="ft col-md-8 col-md-offset-2" value="<?php echo $account;?>"/>
	    		<span class="col-md-8 col-md-offset-2" style="color:red;padding: 5px;"><?php echo $Err ?></span>
	    		<br/><br/><br/>
	    		<input type="text" name="customerid" required placeholder="Customer ID Number" class="ft col-md-8 col-md-offset-2" value="<?php echo $customerid;?>"/><br/><br/><br/>
	    		<input type="text" name="customeric" required placeholder="Customer IC" class="ft col-md-8 col-md-offset-2" value="<?php echo $customeric;?>"/><br/><br/><br/>

	    		<select name="actype" required class="ft col-md-8 col-md-offset-2">
	    			<option disabled="disabled" selected>Account Type</option>
	    			<option value="Excel">Excel</option>
	    			<option value="Power">Power</option>
	    			<option value="Power Plus">Power Plus</option>
	    			<option value="Savings">Savings</option>
	    			<option value="Current">Current</option>
	    			<option value="Power">Power</option>
	    			<option value="FC">FC</option>
	    			<option value="RFCD">RFCD</option>
	    		</select><br/><br/><br/>
	    		
	    		<select name="currency" required class="ft col-md-8 col-md-offset-2">
	    			<option disabled="disabled" selected>Currency</option>
	    			<option value="Taka">Taka</option>
	    			<option value="Dollar">Dollar</option>
	    			<option value="Euro">Euro</option>
	    			<option value="GBP">GBP</option>
	    		</select><br/><br/><br/>
	    		<input type="text" name="initial" required placeholder="Initial Deposit" class="ft col-md-8 col-md-offset-2" value="<?php echo $initial;?>"/><br/><br/><br/>
	    		<input type="submit" name="submit" value="Submit" class="ftb col-md-8 col-md-offset-2"><br/>
	    		<br/><br/><a href="account.php" class="col-md-2 col-md-offset-5">Back</a>
	    		<br/><br/><br/><br/>
	    	</form>
	    </div>
	</div><br/>
</body>
</html>