<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
	$search="";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin|Transection For Today</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">Admin Panel/Account Information</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-3">
					<ul class="nav navbar-nav">
						<li><a href="admin.php">Admin</a></li>
						<li><a href="deposit.php">Deposit</a></li>
						<li><a href="withdraw.php">Withdraw</a></li>
						<li><a href="account.php">Open New Account</a></li>
						<li><a href="addAdmin.php">Add New Admin</a></li>
						<li><a href="checkallac.php">Check All Account</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	<table class="table" id="tab" align="center">
	    <br/>
	    <thead>
		      	<tr>
		      		<th>Total Deposit</th>
			        <th>Total Withdraw</th>
			        <th>Balance</th>
		      	</tr>
		    </thead>
		    <tbody>
		    <?php
				$con = odbc_connect('BMS', 'system', 'prabal');
				if(!$con)
				{
					die('Database not connected!');
				}
				date_default_timezone_set("Asia/Dhaka");
				$dat = date("Y/m/d");
				$sql = "SELECT SUM(DEPOSIT) AS DEPOSIT,SUM(WITHDRAW) AS WITHDRAW,SUM(TOTAL) AS TOTAL FROM transection where TODATE = '".$dat."'";
				$result = odbc_exec($con, $sql);
				echo "<h2 align=center>Transection Record</h2>";
				$row = odbc_fetch_array($result);
				$balance = $row['DEPOSIT'] - $row['WITHDRAW'];
				?>
		      	<tr class="success">
			        <td><?php echo $row['DEPOSIT'];?></td>
			        <td><?php echo $row['WITHDRAW'];?></td>
			        <td><?php echo $balance;?></td>
		     	</tr>
		    <?php
				
			?>
		    </tbody>
	    </table>


	    <table class="table" id="tab" align="center">
	    <br/>
		    <thead>
		      	<tr>
		      		<th>Account No</th>
			        <th>Deposit</th>
			        <th>Withdraw</th>
			        <th>Balance</th>
			        <th>Day</th>
			        <th>Time</th>
			        <th>Date</th>
		      	</tr>
		    </thead>
		    <tbody>
		    <?php
		    	date_default_timezone_set("Asia/Dhaka");
				$dat = date("Y/m/d");
				$con = odbc_connect('BMS', 'system', 'prabal');
				if(!$con)
				{
					die('Database not connected!');
				}
				$sql = "SELECT * FROM transection where TODATE = '".$dat."'";
				$result = odbc_exec($con, $sql);
				echo "<h2 align=center>Transection For Today</h2>";
				while($row = odbc_fetch_array($result)) {
				?>
		      	<tr class="success">
		      		<td><?php echo $row['ACCOUNTNO'];?></td>
			        <td><?php echo $row['DEPOSIT'];?></td>
			        <td><?php echo $row['WITHDRAW'];?></td>
			        <td><?php echo $row['TOTAL'];?></td>
			        <td><?php echo $row['TODAY'];?></td>
			        <td><?php echo $row['TOTIME'];?></td>
			        <td><?php echo $row['TODATE'];?></td>
		     	</tr>
		    <?php
				}
			?>
		    </tbody>

	  	</table><br/>
	  	<a style="margin-left: 10%;" href="trnReport.php">PDF</a> | <a href="admin.php">Home</a>
	  	<br/><br/>
	</div>
</body>
</html>