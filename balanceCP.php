<?php
	require("pdf/fpdf.php");
	$ids = $_GET['ac'];

	$pdf = new FPDF('P','mm','Letter');
	$pdf->SetMargins(16,10);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	
	$pdf->SetFont("Arial","B",12);
	$pdf->Cell(0,10,"Balance Report $ids",0,1,'C');
	//$pdf->Ln();
	

	$pdf->Cell(60,10,"Total Deposit",1,0,'C');
	$pdf->Cell(60,10,"Total Withdraw",1,0,'C');
	$pdf->Cell(65,10,"Balance",1,1,'C');

	$con = odbc_connect('BMS', 'system', 'prabal');
	if(!$con)
	{
		die('Database not connected!');
	}
	$sql1 = "SELECT SUM(DEPOSIT) AS DEPOSIT,SUM(WITHDRAW) AS WITHDRAW,SUM(TOTAL) AS TOTAL FROM transection where ACCOUNTNO = '".$ids."'";
	$results = odbc_exec($con, $sql1);
	$rows = odbc_fetch_array($results);
	$balance = $rows['DEPOSIT'] - $rows['WITHDRAW'];
	
	$pdf->SetFont("Arial","B",8);
	$pdf->Cell(60,10,$rows['DEPOSIT'],1,0,'C');
	$pdf->Cell(60,10,$rows['WITHDRAW'],1,0,'C');
	$pdf->Cell(65,10,$balance,1,1,'C');

	$pdf->Output();

?>