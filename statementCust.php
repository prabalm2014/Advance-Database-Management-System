<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Statement</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">User Panel/Statement</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-4">
					<ul class="nav navbar-nav">
						<li><a href="customer.php">Home</a></li>
						<li><a href="updatePass.php">Update Password</a></li>
						<li><a href="balanceCust.php">Balance</a></li>
						<li><a href="topup.php">Mobile Topup</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	<h2 align=center>Bank Statement</h2><br/>
	<table class="table" id="tab" align="center">
	<br/>
	    <thead>
	      	<tr>
		        <th>Account No</th>
		        <th>Deposit</th>
		        <th>Withdraw</th>
		        <th>Balance</th>
		        <th>Day</th>
		        <th>Time</th>
		        <th>Date</th>
	      	</tr>
	    </thead>
	    <tbody>
	    <?php
	    	$srt = $_SESSION["username"];
			$con = odbc_connect('BMS', 'system', 'prabal');
			if(!$con)
			{
				die('Database not connected!');
			}
			$sql = "SELECT * FROM transection where accountno = '".$srt."' ORDER BY T_ID DESC";
			$result = odbc_exec($con, $sql);
			while($row = odbc_fetch_array($result)) {
			?>
	      	<tr class="success">
		        <td><?php echo $row['ACCOUNTNO'];?></td>
		        <td><?php echo $row['DEPOSIT'];?></td>
		        <td><?php echo $row['WITHDRAW'];?></td>
		        <td><?php echo $row['TOTAL'];?></td>
		        <td><?php echo $row['TODAY'];?></td>
		        <td><?php echo $row['TOTIME'];?></td>
		        <td><?php echo $row['TODATE'];?></td>
	     	</tr>
	    <?php	
		}
		?>
	    </tbody>
	</table><br/>
	<?php
	  	if(!empty($srt))
	  	{
	  		echo "<a style='margin-left: 10%;'' href='statementCP.php?ac=$srt'>PDF</a><br/><br/>";
	  	}
		?>
	</div>
</body>
</html>