<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
	$Err= $Err1=$Err2=$Err3=$Err4="";
	$newpass1 = $newpass2 =  $oldpass = "";
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$acno = $_SESSION["username"];
		$ok=1;
		if(empty($_POST["newpass1"]) && empty($_POST["newpass2"]) && empty($_POST["oldpass"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$newpass1 = test_input($_POST["newpass1"]);
			$newpass2 = test_input($_POST["newpass2"]);
			$oldpass = test_input($_POST["oldpass"]);
		}
		if(!empty($_POST["submit"]))
		{	
			$con = odbc_connect('BMS', 'system', 'prabal');
			if(!$con)
			{
				die('Database not connected!');
			}
			if($newpass1 == $newpass2)
			{
				$sql2 = "SELECT PASSWORD FROM users WHERE ACCOUNTNO='".$acno."' ";
				$results = odbc_exec($con, $sql2);
				$rows = odbc_fetch_array($results);
				if($rows['PASSWORD'] == $oldpass)
				{
					$sql= "UPDATE users SET PASSWORD = '$newpass2' WHERE ACCOUNTNO = '$acno' ";
					odbc_exec($con, $sql);
					header('Location: customer.php');
					exit();
				}
				else
				{
					$Err1 = "Old Password You Entered Not Matched !";
					$oldpass ="";
				}
			}
			else
			{
				$Err1 = "Password You Entered Not Matched !";
				$newpass2 ="";
			}
		}
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>User|Update Password</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">User Panel/Update Password</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-4">
					<ul class="nav navbar-nav">
						<li><a href="customer.php">Home</a></li>
						<li><a href="balanceCust.php">Balance</a></li>
						<li><a href="statementCust.php">Statement</a></li>
						<li><a href="topup.php">Mobile Topup</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
    	<div class="col-md-6 col-md-offset-3">
	    	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
	    		<span class="col-md-8 col-md-offset-2" style="color:red;padding: 5px;"><h4><?php echo $Err.$Err1.$Err2.$Err3.$Err4 ?></h4></span><br/><br/><br/>
	    		<h1 class="h1-3 col-md-offset-2">Update Password</h1><br/>

	    		<input type="password" name="newpass1" required placeholder="New Password" class="ft col-md-6 col-md-offset-2" value="<?php echo $newpass1?>"/><br/><br/><br/>

	    		<input type="password" name="newpass2" required placeholder="Re-Type New Password" class="ft col-md-6 col-md-offset-2" value="<?php echo $newpass2?>"/><br/><br/><br/>

	    		<input type="password" name="oldpass" required placeholder="Old Password" class="ft col-md-6 col-md-offset-2" value="<?php echo $oldpass?>"/><br/><br/><br/>
	    		
	    		<input type="submit" name="submit" value="Update" class="ftb col-md-4 col-md-offset-2"><br/>
	    		<br/><br/><br/><a href="customer.php" class="col-md-1 col-md-offset-2">Back</a>
	    		<br/>
	    		<br/><br/><br/>
	    	</form>
	    </div>
	</div>
</body>
</html>