<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Balance</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">User Panel/Balance</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-4">
					<ul class="nav navbar-nav">
						<li><a href="customer.php">Home</a></li>
						<li><a href="updatePass.php">Update Password</a></li>
						<li><a href="statementCust.php">Statement</a></li>
						<li><a href="topup.php">Mobile Topup</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	
	<table class="table" id="tab" align="center">
	    <br/><br/><br/><br/><br/>
	    	<thead>
		      	<tr>
		      		<th>Total Deposit</th>
			        <th>Total Withdraw</th>
			        <th>Balance</th>
		      	</tr>
		    </thead>
		    <tbody>
		    <?php
				$srt = $_SESSION["username"];
				$con = odbc_connect('BMS', 'system', 'prabal');
				if(!$con)
				{
					die('Database not connected!');
				}
				
				$sql = "SELECT SUM(DEPOSIT) AS DEPOSIT,SUM(WITHDRAW) AS WITHDRAW,SUM(TOTAL) AS TOTAL FROM transection where ACCOUNTNO = '".$srt."' ";
				$result = odbc_exec($con, $sql);
				echo "<h2 align=center>Balance for ".$srt."</h2><br/>";
				$row = odbc_fetch_array($result);
				$balance = $row['DEPOSIT'] - $row['WITHDRAW'];
				?>
		      	<tr class="success">
			        <td><?php echo $row['DEPOSIT'];?></td>
			        <td><?php echo $row['WITHDRAW'];?></td>
			        <td><?php echo $balance;?></td>
		     	</tr>
		    <?php
			?>
		    </tbody>
	    </table>
	  	<?php
	  	if(!empty($srt))
	  	{
	  		echo "<a style='margin-left: 10%;'' href='balanceCP.php?ac=$srt'>PDF</a><br/><br/>";
	  	}
		?>
	
	</div>
</body>
</html>