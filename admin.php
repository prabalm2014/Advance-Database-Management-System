<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
	$search="";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">Admin Panel</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-3">
					<ul class="nav navbar-nav">
						<li><a href="deposit.php">Deposit</a></li>
						<li><a href="withdraw.php">Withdraw</a></li>
						<li><a href="account.php">Open New Account</a></li>
						<li><a href="addAdmin.php">Add New Admin</a></li>
						<li><a href="checkallac.php">Check All Account</a></li>
						<li><a href="transection.php">Transection For Today</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
    	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>"><br/>
    		<input type="text" name="search" placeholder="Search For Account" class="fts col-md-5 col-md-offset-3" value="<?php echo $search?>"/>
    		<input type="submit" name="submit" class="bts col-md-1" value="Search"/>
    	</form>
	    <table class="table" id="tab" align="center">
	    <br/><br/><br/><br/>
		    <thead>
		      	<tr>
			        <th>Account No</th>
			        <th>Deposit</th>
			        <th>Withdraw</th>
			        <th>Balance</th>
			        <th>Day</th>
			        <th>Time</th>
			        <th>Date</th>
		      	</tr>
		    </thead>
		    <tbody>
		    <?php
		    if(empty($_POST['search']))
		    {
		    	echo "<h2 align=center>No Account To Show!</h2>";
		    }
			else if(!empty($_POST['search']))
			{
				$srt = $_POST['search'];
				$con = odbc_connect('BMS', 'system', 'prabal');
				if(!$con)
				{
					die('Database not connected!');
				}
				$sql = "SELECT * FROM transection where accountno like '".$srt."%' ORDER BY TODATE DESC";
				$result = odbc_exec($con, $sql);
				echo "<h2 align=center>Search Result</h2>";
				while($row = odbc_fetch_array($result)) {
				?>
		      	<tr class="success">
			        <td><?php echo $row['ACCOUNTNO'];?></td>
			        <td><?php echo $row['DEPOSIT'];?></td>
			        <td><?php echo $row['WITHDRAW'];?></td>
			        <td><?php echo $row['TOTAL'];?></td>
			        <td><?php echo $row['TODAY'];?></td>
			        <td><?php echo $row['TOTIME'];?></td>
			        <td><?php echo $row['TODATE'];?></td>
		     	</tr>
		    <?php
				}
			}
			?>
		    </tbody>
	  	</table><br/>


	  	<table class="table" id="tab" align="center">
	    <br/>
	    	<thead>
		      	<tr>
		      		<th>Total Deposit</th>
			        <th>Total Withdraw</th>
			        <th>Balance</th>
		      	</tr>
		    </thead>
		    <tbody>
		    <?php
		    if(empty($_POST['search']))
		    {
		    	echo "<h2 align=center>Balance</h2>";
		    }
		    else if(!empty($_POST['search']))
			{
				$srt = $_POST['search'];
				$con = odbc_connect('BMS', 'system', 'prabal');
				if(!$con)
				{
					die('Database not connected!');
				}
				date_default_timezone_set("Asia/Dhaka");
				$dat = date("Y/m/d");
				$sql = "SELECT SUM(DEPOSIT) AS DEPOSIT,SUM(WITHDRAW) AS WITHDRAW,SUM(TOTAL) AS TOTAL FROM transection where ACCOUNTNO like '".$srt."%' ";
				$result = odbc_exec($con, $sql);
				echo "<h2 align=center>Balance</h2>";
				$row = odbc_fetch_array($result);
				$balance = $row['DEPOSIT'] - $row['WITHDRAW'];
				?>
		      	<tr class="success">
			        <td><?php echo $row['DEPOSIT'];?></td>
			        <td><?php echo $row['WITHDRAW'];?></td>
			        <td><?php echo $balance;?></td>
		     	</tr>
		    <?php
			}
			?>
		    </tbody>
	    </table>
	  	<?php
	  	if(!empty($srt))
	  	{
	  		echo "<a style='margin-left: 10%;'' href='srchReport.php?ac=$srt'>PDF</a><br/><br/>";
	  	}
		?>
	  
	</div>
</body>
</html>