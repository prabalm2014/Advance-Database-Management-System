<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
	$Err= $Err1=$Err2=$Err3=$Err4="";
	$phone1 = $phone2 =  $amount1 =  $amount2 = $pass = "";
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		date_default_timezone_set("Asia/Dhaka");
		$time = date("h:i:sa");
		$dat = date("Y/m/d");
		$mydate = getdate(date("U"));
		$day = "$mydate[weekday]";
		$acno = $_SESSION["username"];
		$ok=1;
		if(empty($_POST["phone1"]) && empty($_POST["phone2"]) && empty($_POST["amount1"]) && empty($_POST["amount2"]) && empty($_POST["pass"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$phone1 = test_input($_POST["phone1"]);
			$phone2 = test_input($_POST["phone2"]);
			$amount1 = test_input($_POST["amount1"]);
			$amount2 = test_input($_POST["amount2"]);
			$pass = test_input($_POST["pass"]);
		}
		if(!empty($_POST["submit"]))
		{	
			$con = odbc_connect('BMS', 'system', 'prabal');
			if(!$con)
			{
				die('Database not connected!');
			}
			if($phone1 == $phone2)
			{
				$sql1 = "SELECT * FROM(SELECT * FROM transection where ACCOUNTNO='".$acno."' ORDER BY T_ID desc)WHERE  rownum <=1";
				$result = odbc_exec($con, $sql1);
				$row = odbc_fetch_array($result);
				$total = $row['TOTAL'] - $amount2;

				$sql2 = "SELECT PASSWORD FROM users WHERE ACCOUNTNO='".$acno."' ";
				$results = odbc_exec($con, $sql2);
				$rows = odbc_fetch_array($results);

				if($amount1 == $amount2)
				{
					if($amount2 <= $row['TOTAL'])
					{
						if($rows['PASSWORD'] == $pass)
						{
							$sql = "INSERT INTO transection VALUES (transection_sequence.nextval,'".$acno."',null,'".$amount2."','".$total."','".$day."','".$time."','".$dat."')";
							odbc_exec($con, $sql);
							header('Location: customer.php');
							exit();
						}
						else
						{
							$Err4 = "Password Don't Matched !";
							$pass = "";
						}
					}
					else
					{
						$Err3 = "You Don't Have Enough Money In Your Account !";
						$amount2 = $amount1 = "";
					}
				}
				else
				{
					$Err2 = "Amount You Entered Not Matched !";
					$amount2 = "";
				}
			}
			else
			{
				$Err1 = "Phone Number You Entered Not Matched !";
				$phone2 ="";
			}
		}
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>User|Top Up</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">User Panel/Top Up</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-4">
					<ul class="nav navbar-nav">
						<li><a href="customer.php">Home</a></li>
						<li><a href="updatePass.php">Update Password</a></li>
						<li><a href="balanceCust.php">Balance</a></li>
						<li><a href="statementCust.php">Statement</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
    	<div class="col-md-6 col-md-offset-3">
	    	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
	    		<span class="col-md-8 col-md-offset-2" style="color:red;padding: 5px;"><h4><?php echo $Err.$Err1.$Err2.$Err3.$Err4 ?></h4></span><br/><br/><br/>
	    		<h1 class="h1-3 col-md-offset-2">Top Up</h1><br/>

	    		<input type="text" name="phone1" required placeholder="Phone Number" class="ft col-md-6 col-md-offset-2" value="<?php echo $phone1?>"/><br/><br/><br/>

	    		<input type="text" name="phone2" required placeholder="Re-Type Phone Number" class="ft col-md-6 col-md-offset-2" value="<?php echo $phone2?>"/><br/><br/><br/>

	    		<input type="text" name="amount1" required placeholder="Amount" class="ft col-md-6 col-md-offset-2" value="<?php echo $amount1?>"/><br/><br/><br/>

	    		<input type="text" name="amount2" required placeholder="Re-Type Amount" class="ft col-md-6 col-md-offset-2" value="<?php echo $amount2?>"/><br/><br/><br/>

	    		<input type="password" name="pass" required placeholder="Password" class="ft col-md-6 col-md-offset-2" value="<?php echo $pass?>"/><br/><br/><br/>
	    		
	    		<input type="submit" name="submit" value="Send" class="ftb col-md-4 col-md-offset-2"><br/>
	    		<br/><br/><br/><a href="customer.php" class="col-md-1 col-md-offset-2">Back</a>
	    		<br/>
	    		<br/><br/><br/>
	    	</form>
	    </div>
	</div>
</body>
</html>