<?php
	require("pdf/fpdf.php");
	$con = odbc_connect('BMS', 'system', 'prabal');
	if(!$con)
	{
		die('Database not connected!');
	}
	$sql = "SELECT * FROM account join users on users.ACCOUNTNO = account.ACCOUNTNO";
	$result = odbc_exec($con, $sql);

	$pdf = new FPDF('P','mm','Letter');
	$pdf->SetMargins(8,10);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	

	$pdf->SetFont("Arial","B",10);
	$pdf->Cell(0,10,"All Account Report",0,1,'C');
	$pdf->Ln();

	$pdf->Cell(30,10,"Name",1,0,'C');
	$pdf->Cell(30,10,"Phone",1,0,'C');
	$pdf->Cell(30,10,"Address",1,0,'C');
	$pdf->Cell(30,10,"Account",1,0,'C');
	$pdf->Cell(28,10,"Customer ID",1,0,'C');
	$pdf->Cell(28,10,"Customer IC",1,0,'C');
	$pdf->Cell(25,10,"Account Type",1,1,'C');


	while($row = odbc_fetch_array($result)) 
	{
		$pdf->SetFont("Arial","B",8);
		$pdf->Cell(30,10,$row['FULLNAME'],1,0,'C');
		$pdf->Cell(30,10,$row['PHONE'],1,0,'C');
		$pdf->Cell(30,10,$row['ADDRESS'],1,0,'C');
		$pdf->Cell(30,10,$row['ACCOUNTNO'],1,0,'C');
		$pdf->Cell(28,10,$row['CUSTOMERID'],1,0,'C');
		$pdf->Cell(28,10,$row['CUSTOMERIC'],1,0,'C');
		$pdf->Cell(25,10,$row['ACCTYPE'],1,1,'C');
	}
	$pdf->Output();
	
?>