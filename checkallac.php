<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: index.php');
	}
	$search="";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin|Account Information</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">Admin Panel/Account Information</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-3">
					<ul class="nav navbar-nav">
						<li><a href="admin.php">Admin</a></li>
						<li><a href="deposit.php">Deposit</a></li>
						<li><a href="withdraw.php">Withdraw</a></li>
						<li><a href="account.php">Open New Account</a></li>
						<li><a href="addAdmin.php">Add New Admin</a></li>
						<li><a href="transection.php">Transection For Today</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	    <table class="table" id="tab" align="center">
	    <br/>
		    <thead>
		      	<tr>
		      		<th>Name</th>
		      		<th>Phone</th>
		      		<th>Address</th>
			        <th>Account No</th>
			        <th>Customer ID</th>
			        <th>Customer IC</th>
			        <th>Account Type</th>
		      	</tr>
		    </thead>
		    <tbody>
		    <?php
				$con = odbc_connect('BMS', 'system', 'prabal');
				if(!$con)
				{
					die('Database not connected!');
				}
				$sql = "SELECT * FROM account join users on users.ACCOUNTNO = account.ACCOUNTNO";
				//$sql = "SELECT * FROM transection ";
				$result = odbc_exec($con, $sql);
				echo "<h2 align=center>Account Information</h2>";
				while($row = odbc_fetch_array($result)) {
				?>
		      	<tr class="success">
		      		<td><?php echo $row['FULLNAME'];?></td>
		      		<td><?php echo $row['PHONE'];?></td>
		      		<td><?php echo $row['ADDRESS'];?></td>
			        <td><?php echo $row['ACCOUNTNO'];?></td>
			        <td><?php echo $row['CUSTOMERID'];?></td>
			        <td><?php echo $row['CUSTOMERIC'];?></td>
			        <td><?php echo $row['ACCTYPE'];?></td>
		     	</tr>
		    <?php
				}
			?>
		    </tbody>
	  </table><br/>
	  <a style="margin-left: 10%;" href="allacc.php">PDF</a> | <a href="admin.php">Home</a>
	  <br/><br/>
	</div>
</body>
</html>