<?php
	session_start();
	$fname =$gender =$address =$phone =$account =$password ="";
	$Err="";
	$con = odbc_connect('BMS', 'system', 'prabal');
	if(!$con)
	{
		die('Database not connected!');
	}
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$ok=1;
		if(empty($_POST["fname"]) && empty($_POST["gender"]) && empty($_POST["address"]) && empty($_POST["phone"]) && empty($_POST["account"]) && empty($_POST["password"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$fname = test_input($_POST["fname"]);
			$gender = test_input($_POST["gender"]);
			$address = test_input($_POST["address"]);
			$phone = test_input($_POST["phone"]);
			$account = test_input($_POST["account"]);
			$password = test_input($_POST["password"]);
		}
		if(!empty($_POST["submitNext"]))
		{	
			/*$_SESSION["fname"] = $fname;
			$_SESSION["gender"] = $gender;
			$_SESSION["address"] = $address;
			$_SESSION["phone"] = $phone;
			$_SESSION["account"] = $account;
			$_SESSION["password"] = $password;*/
			$sql = "SELECT * FROM ADMIN where USERNAME='".$account."' ";
			$result = odbc_exec($con, $sql);
			$row = odbc_fetch_array($result);
			if($row['USERNAME'] == $account)
			{
				$Err= "&#9932 Account Already Exists!";$ok=0;
				$account ="";
			}
			else
			{
				$sql2 = "INSERT INTO ADMIN VALUES (admin_sequence.nextval,'".$fname."','".$gender."','".$address."','".$phone."','".$account."','".$password."')";
				odbc_exec($con, $sql2);
				header('Location: admin.php');
				exit();
			}
			
		}
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add New Admin</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		    <div class="col-md-0 col-md-offset-1"><h1 class="h1-1">Add New Admin</h1></div>
		</div>
	</div><br/>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-0 col-md-offset-9">
					<ul class="nav navbar-nav">
						<li><a href="admin.php">Admin</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Help</a></li>
						<li><a href="logout.php">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
	    <div class="col-md-6 forms col-md-offset-3">
	    	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>"><br/>
	    		<h1 class="h1-2 col-md-offset-2">Add New Admin</h1><br/>
	    		<input type="text" name="fname" required placeholder="Fullname" class="ft col-md-8 col-md-offset-2" value="<?php echo $fname?>"/><br/><br/><br/>
	    		<select name="gender" required class="ft col-md-8 col-md-offset-2" value="<?php echo $gender?>" >
	    			<option disabled="disabled" selected>Gender</option>
	    			<option value="Male">Male</option>
	    			<option value="Female">Female</option>
	    		</select><br/><br/><br/>
	    		<input type="text" name="address" required placeholder="Address" class="ft col-md-8 col-md-offset-2" value="<?php echo $address?>"/><br/><br/><br/>
	    		<input type="text" name="phone" required placeholder="Phone" class="ft col-md-8 col-md-offset-2" value="<?php echo $phone?>"/><br/><br/><br/>
	    		<input type="text" name="account" required placeholder="Username" class="ft col-md-8 col-md-offset-2" value="<?php echo $account?>"/>
	    		<span class="col-md-8 col-md-offset-2" style="color:red;padding: 5px;"><?php echo $Err ?></span>
	    		<br/><br/><br/>
	    		<input type="password" name="password" required placeholder="Password" class="ft col-md-8 col-md-offset-2" value="<?php echo $password?>"/><br/><br/><br/><br/>
	    		<input type="submit" name="submitNext" value="Next" class="ftb col-md-8 col-md-offset-2"><br/><br/>
	    		<br/><br/><a href="admin.php" class="col-md-2 col-md-offset-5">Back</a>
	    		<br/><br/><br/><br/>
	    	</form>
	    </div>
	</div><br/>
</body>
</html>